package com.example.veriko.botemil;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InsertarActivity extends AppCompatActivity implements View.OnClickListener {

    EditText participante;
    Button guardar;
    SQLiteDatabase bd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar);

        Conexion conexion=new Conexion(this, "bote", null, 1);
        bd=conexion.getWritableDatabase();

        participante=(EditText)findViewById(R.id.participante);
        guardar=(Button)findViewById(R.id.guardarParticipante);

        guardar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        bd.execSQL("INSERT INTO participantes (nombreParticipante) VALUES ('"+participante.getText().toString()+"')");
        finish();
    }
}
