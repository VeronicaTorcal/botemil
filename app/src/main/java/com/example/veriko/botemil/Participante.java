package com.example.veriko.botemil;

/**
 * Created by Veriko on 30/05/2017.
 */

public class Participante {

    int idParticipante;
    String nombreParticipante;

    public Participante(int idParticipante, String nombreParticipante) {
        this.idParticipante = idParticipante;
        this.nombreParticipante = nombreParticipante;
    }

    public int getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(int idParticipante) {
        this.idParticipante = idParticipante;
    }

    public String getNombreParticipante() {
        return nombreParticipante;
    }

    public void setNombreParticipante(String nombreParticipante) {
        this.nombreParticipante = nombreParticipante;
    }
}
