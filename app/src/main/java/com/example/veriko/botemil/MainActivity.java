package com.example.veriko.botemil;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button listado, insertarParticipante, insertarActividad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listado=(Button)findViewById(R.id.listado);
        insertarParticipante=(Button)findViewById(R.id.insertarParticipante);
        insertarActividad=(Button)findViewById(R.id.insertarActividad);

        // implementamos los botones POR SEPARADO
        listado.setOnClickListener(this);
        insertarParticipante.setOnClickListener(this);
        insertarActividad.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent i;
        switch (v.getId()){
            case R.id.listado:
                i=new Intent(this, ListadoActivity.class);
                startActivity(i);
                break;
            case R.id.insertarParticipante:
                i=new Intent(this, InsertarActivity.class);
                startActivity(i);
                break;
            case R.id.insertarActividad:
                i=new Intent(this, InsertarActividadActivity.class);
                startActivity(i);
                break;
        }

    }
}
