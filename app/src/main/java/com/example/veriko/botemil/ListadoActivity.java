package com.example.veriko.botemil;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ListadoActivity extends AppCompatActivity implements View.OnClickListener {

    TextView listado, listadoActividades;
    Button quienDebe, volver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);
        listado=(TextView)findViewById(R.id.mostrarListado);
        listadoActividades=(TextView)findViewById(R.id.mostrarListadoActividades);
        quienDebe=(Button)findViewById(R.id.quienDebe);
        volver=(Button)findViewById(R.id.volver);

        quienDebe.setOnClickListener(this);
        volver.setOnClickListener(this);

        Conexion conexion=new Conexion(this, "bote", null, 1);
        // instancio en modo lectura
        SQLiteDatabase bd=conexion.getReadableDatabase();

        // creo un cursor que recogera los nombres de los amigos
        Cursor cursor=bd.rawQuery("SELECT * FROM participantes", null);

        // pongo el cursor en primera posicion
        cursor.moveToFirst();
        String fila="";
        do {
            fila+=cursor.getString(cursor.getColumnIndex("nombreParticipante"))+"\n ";

        }while(cursor.moveToNext());

        listado.setText(fila);

//        OTRO CURSOR IGUAL CON OTROS NOMBRES, PARA LAS ACTIVIDADES

        Cursor cursorA=bd.rawQuery("SELECT * FROM actividades", null);

        // pongo el cursor en primera posicion
        cursorA.moveToFirst();
        String filaA="";
        do {
            filaA+=cursorA.getString(cursorA.getColumnIndex("nombreActividad"))+" -> "+cursorA.getString(cursorA.getColumnIndex("precioActividad"))+" -> "+cursorA.getString(cursorA.getColumnIndex("idParticipante"))+"\n ";

        }while(cursorA.moveToNext());

        listadoActividades.setText(filaA);
    }

    @Override
    public void onClick(View v) {

        Intent i;

        if (v.getId()==R.id.quienDebe){
            i=new Intent(this, ResultadoActivity.class);
            startActivity(i);
        }else {
            i=new Intent( Intent.ACTION_MAIN); //Llamando a la activity principal
            finish();
        }

    }
}
