package com.example.veriko.botemil;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ResultadoActivity extends AppCompatActivity {

    ListView mostrarDeudas;
    ArrayList<String> listaParticipantes;
    ArrayAdapter adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        mostrarDeudas=(ListView) findViewById(R.id.mostrarDeudas);
        Conexion conexion=new Conexion(this, "bote", null, 1);
        // instancio en modo lectura
        SQLiteDatabase bd=conexion.getReadableDatabase();

        // creo un cursor que recogerá las actividades
        Cursor cursorA=bd.rawQuery("SELECT * FROM actividades", null);
        // habrá que hacer otro cursor de la otra tabla para que luego aparezcan los nombres, no los ids.
        Cursor cursorP=bd.rawQuery("SELECT * FROM participantes", null);
       // startManagingCursor(cursorP);
        int contadorParticipantes=0;
        if (cursorP != null){
            // contadorParticipantes ya tiene el numero de participantes. ahora hace falta el dinero total
            contadorParticipantes=cursorP.getCount();
            System.out.println("-> "+contadorParticipantes);
        }else {
            System.out.println("-> no hay participantes");
        }
        double totalGastado=0;
        String total="";
        cursorA.moveToFirst();
        do {
            total=cursorA.getString(cursorA.getColumnIndex("precioActividad"));
            totalGastado+=Double.valueOf(total).doubleValue();

        }while(cursorA.moveToNext());
        System.out.println("-> total:"+totalGastado);
        //  YA TENGO EL TOTAL Y EL NUMERO DE AMIGOS A DIVIDIR
        double mediaDinero=totalGastado/contadorParticipantes;
        System.out.println("-> media:"+ mediaDinero);
//       HASTA AHORA TENGO EL METODO llenarListView EN CONEXION PARA RELLENAR EL LISTVIEW.
//       FALTA QUE, DESDE AHI, DEPENDIENDO DE CUANTO HAYA PAGADO LA PERSONA PONGA DETRAS, "DEBE..." O "LE DEBEN..."
        listaParticipantes= conexion.llenarListView(mediaDinero);
        adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaParticipantes);
        mostrarDeudas.setAdapter(adaptador);

    }
}
