package com.example.veriko.botemil;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Veriko on 30/05/2017.
 */

public class Conexion extends SQLiteOpenHelper {

    String SQLParticipantes="CREATE TABLE participantes (idParticipante integer primary key autoincrement, nombreParticipante varchar(30))";
    String SQLActividades="CREATE TABLE actividades (idActividad integer primary key autoincrement, nombreActividad varchar(30), precioActividad double (6,2), idParticipante integer, FOREIGN KEY (idParticipante) REFERENCES participantes(idParticipante))";

    String SQLEmpleados="CREATE TABLE empleados (codigo integer primary key autoincrement,nombre varchar(15), apellidos varchar(25),departamento int)";

    public Conexion(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQLParticipantes);
        db.execSQL(SQLActividades);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE if exist participantes");
        db.execSQL("DROP TABLE if exists actividades");

        db.execSQL(SQLParticipantes);
        db.execSQL(SQLActividades);

    }

    // vamos a probar a hacer un metodo aqui para rellenar el arrayList
    // quito el contadorParticipantes, que no me hace falta
    public ArrayList llenarListView(double mediaDinero){

        ArrayList<String> lista = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String b="SELECT participantes.nombreParticipante, SUM(actividades.precioActividad) AS suma FROM participantes, actividades WHERE actividades.idParticipante=participantes.idParticipante GROUP BY participantes.idParticipante";
        Cursor resultados = db.rawQuery(b, null);
        resultados.moveToFirst();
        lista.add("La media es: "+ mediaDinero);

        do {

//              YA CONSIGO QUE DIGA CUANTO HAN PAGADO Y CUANTO DEBEN.
//            AHORA AQUI DENTRO TENGO QUE DECIR YA MISMO, A QUIEN LE DEBE
            if (resultados.getDouble(resultados.getColumnIndex("suma")) < mediaDinero){

                lista.add(resultados.getString(resultados.getColumnIndex("nombreParticipante"))+" ha pagado: "+ resultados.getString(resultados.getColumnIndex("suma"))+ " debe "+ (mediaDinero-resultados.getDouble(resultados.getColumnIndex("suma"))));
            } else {
                lista.add(resultados.getString(resultados.getColumnIndex("nombreParticipante"))+" ha pagado: "+ resultados.getString(resultados.getColumnIndex("suma"))+ " le deben "+ (resultados.getDouble(resultados.getColumnIndex("suma"))-mediaDinero));
            }

        }while (resultados.moveToNext());

        return lista;
    }
//    ESTA FUNCION TODAVÍA NO TIENE LLAMADA A ELLA DESDE RESULTADOACTIVITY
//    LAS 5 PRIMERAS LINEAS SON IGUALES QUE LA FUNCION ANTERIOR, ES PARA SEPARAR LO QUE YA FUNCIONA DE LO QUE NO
    public ArrayList quienDebeAquien(double mediaDinero){

        ArrayList<String> lista = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String b="SELECT participantes.nombreParticipante, SUM(actividades.precioActividad) AS suma FROM participantes, actividades WHERE actividades.idParticipante=participantes.idParticipante GROUP BY participantes.idParticipante";
        Cursor resultados = db.rawQuery(b, null);
        resultados.moveToFirst();
        lista.add("QUIEN DEBE A QUIEN: ");
//        ¿HABRIA QUE HACER UN ARRAY DE AMIGOS A LOS QUE SE LES DEBE PASTA Y OTRO DE LOS QUE DEBEN O ALGO ASI?

        do {



        }while (resultados.moveToNext());

        return lista;
    }
}


