package com.example.veriko.botemil;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class InsertarActividadActivity extends AppCompatActivity  {

    ArrayList<Participante> obParticipante=new ArrayList<Participante>();
    ArrayList<String> participantes=new ArrayList<String>();

    EditText nombreActividad, precioActividad;
    Spinner spinnerParticipante;
    Button guardarActividad;

    SQLiteDatabase bd;
    Participante participante;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar_actividad);
        nombreActividad=(EditText)findViewById(R.id.nombreActividad);
        precioActividad=(EditText)findViewById(R.id.precioActividad);
        spinnerParticipante=(Spinner)findViewById(R.id.desplegable);
        guardarActividad=(Button)findViewById(R.id.guardarActividad);

        Conexion conexion=new Conexion(this, "bote", null, 1);
        bd=conexion.getWritableDatabase();

        // hago consulta para recoger los participantes
        Cursor cursor=bd.rawQuery("SELECT * FROM participantes", null);
        // recorro el cursor y meto datos al array de strings y de objetos
        cursor.moveToFirst();
        do {
           participantes.add(cursor.getString(cursor.getColumnIndex("nombreParticipante")));
           obParticipante.add(new Participante(cursor.getInt(cursor.getColumnIndex("idParticipante")),cursor.getString(cursor.getColumnIndex("nombreParticipante"))));
        }while (cursor.moveToNext());

        // creo un arrayadapter para inflar el spinner
        final ArrayAdapter adaptador=new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, participantes);
        // seteo el adaptador
        spinnerParticipante.setAdapter(adaptador);

        guardarActividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bd.execSQL("INSERT INTO actividades (nombreActividad, precioActividad, idParticipante) values " + "('"+nombreActividad.getText().toString()+"',"+Double.valueOf(precioActividad.getText().toString())+", "+Integer.valueOf(participante.getIdParticipante())+")");                finish();
            }
        });

        spinnerParticipante.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                participante=obParticipante.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


}
